package ru.buzanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.service.TaskService;

import java.util.Collection;

@RestController
@RequestMapping(value = "/rest/{userId}/task")
public class TaskRestController {
    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<Collection<TaskDTO>> listTask(@PathVariable(name = "userId") String userId) throws Exception {
        return ResponseEntity.ok(taskService.findAll(userId));
    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<TaskDTO> getTask(@PathVariable(name = "userId") String userId, @PathVariable final String id) throws Exception {
        return ResponseEntity.ok(taskService.findOne(userId, id));
    }

    @RequestMapping(value = "/mergeAll", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<TaskDTO>> mergeAllTask(@PathVariable(name = "userId") String userId, @RequestBody Collection<TaskDTO> tasks) throws Exception {
        for (TaskDTO task : tasks)
            taskService.merge(userId, task.getId(), task);
        return ResponseEntity.ok(taskService.findAll(userId));
    }

    @RequestMapping(value = "/merge", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskDTO> mergeTask(@PathVariable(name = "userId") String userId, @RequestBody TaskDTO task) throws Exception {
        taskService.merge(userId, task.getId(), task);
        return ResponseEntity.ok(taskService.findOne(userId, task.getId()));
    }

    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity removeTask(@PathVariable(name = "userId") String userId, @PathVariable final String id) throws Exception {
        taskService.remove(userId, id);
        return ResponseEntity.ok("Ok");
    }

    @RequestMapping(value = "/removeAll", method = RequestMethod.DELETE)
    public ResponseEntity removeAllTask(@PathVariable(name = "userId") String userId) throws Exception {
        taskService.removeAll(userId);
        return ResponseEntity.ok("Ok");
    }
}
