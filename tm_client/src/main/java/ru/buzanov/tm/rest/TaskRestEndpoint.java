package ru.buzanov.tm.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.buzanov.tm.dto.TaskDTO;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Component
public class TaskRestEndpoint {
    @NotNull
    final String adress = "http://localhost:8080/rest/b8f39109-706f-445b-b3c3-a25ad32266ad/task/";

    public void merge(@NotNull final TaskDTO taskDTO) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.put(new URI(adress + "merge/"), taskDTO);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void mergeAll(final Collection<TaskDTO> tasks) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.put(new URI(adress + "mergeAll/"), tasks);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public TaskDTO findOne(final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate.getForObject(new URI(adress + "find/" + id), TaskDTO.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<TaskDTO> findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            final TaskDTO[] taskDTOS = restTemplate.getForObject(new URI(adress + "findAll"), TaskDTO[].class);
            if (taskDTOS == null)
                return null;
            return Arrays.asList(taskDTOS);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void removeOne(final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.delete(new URI(adress + "remove/" + id));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void removeAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.delete(new URI(adress + "removeAll"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

}
