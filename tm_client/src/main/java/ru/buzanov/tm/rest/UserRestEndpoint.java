package ru.buzanov.tm.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.buzanov.tm.dto.UserDTO;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Component
public class UserRestEndpoint {
    @NotNull
    final String adress = "http://localhost:8080/rest/user/";

    public void merge(@NotNull final UserDTO userDTO) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.put(new URI(adress + "merge/"), userDTO);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void mergeAll(final Collection<UserDTO> users) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.put(new URI(adress + "mergeAll/"), users);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public UserDTO findOne(final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate.getForObject(new URI(adress + "find/" + id), UserDTO.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<UserDTO> findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            final UserDTO[] userDTOS = restTemplate.getForObject(new URI(adress + "findAll"), UserDTO[].class);
            if (userDTOS == null)
                return null;
            return Arrays.asList(userDTOS);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void removeOne(final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.delete(new URI(adress + "remove/" + id));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void removeAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.delete(new URI(adress + "removeAll"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

}
