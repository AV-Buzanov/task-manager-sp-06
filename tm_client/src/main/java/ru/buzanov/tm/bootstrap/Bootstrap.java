package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.rest.ProjectRestEndpoint;
import ru.buzanov.tm.rest.TaskRestEndpoint;
import ru.buzanov.tm.rest.UserRestEndpoint;

@Getter
@Setter
@Component
@NoArgsConstructor
public final class Bootstrap {
    @NotNull
    @Autowired
    private ProjectRestEndpoint projectRestEndpoint;
    @Autowired
    private TaskRestEndpoint taskRestEndpoint;
    @Autowired
    private UserRestEndpoint userRestEndpoint;

    public void start() {

    }

}