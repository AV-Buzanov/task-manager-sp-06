package ru.buzanov.tm.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.buzanov.tm.dto.ProjectDTO;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Component
public class ProjectRestEndpoint {
    @NotNull
    final String adress = "http://localhost:8080/rest/b8f39109-706f-445b-b3c3-a25ad32266ad/project/";

    public void merge(@NotNull final ProjectDTO projectDTO) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.put(new URI(adress + "merge/"), projectDTO);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void mergeAll(final Collection<ProjectDTO> projects) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.put(new URI(adress + "mergeAll/"), projects);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public ProjectDTO findOne(final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate.getForObject(new URI(adress + "find/" + id), ProjectDTO.class);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<ProjectDTO> findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            final ProjectDTO[] projectDTOS = restTemplate.getForObject(new URI(adress + "findAll"), ProjectDTO[].class);
            if (projectDTOS == null)
                return null;
            return Arrays.asList(projectDTOS);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void removeOne(final String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.delete(new URI(adress + "remove/" + id));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void removeAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.delete(new URI(adress + "removeAll"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

}
