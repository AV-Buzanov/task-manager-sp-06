package ru.buzanov.tm.controller;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.service.ProjectService;

import java.util.Collection;

@NoArgsConstructor
@RestController
@RequestMapping(value = "/rest/{userId}/project")
public class ProjectRestController {
    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<Collection<ProjectDTO>> listProject(@PathVariable(name = "userId") String userId) throws Exception {
        return ResponseEntity.ok(projectService.findAll(userId));
    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProjectDTO> getProject(@PathVariable(name = "userId") String userId, @PathVariable final String id) throws Exception {
        return ResponseEntity.ok(projectService.findOne(userId, id));
    }

    @RequestMapping(value = "/mergeAll", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<ProjectDTO>> mergeAllProject(@PathVariable(name = "userId") String userId, @RequestBody Collection<ProjectDTO> projects) throws Exception {
        for (ProjectDTO project : projects)
            projectService.merge(userId, project.getId(), project);
        return ResponseEntity.ok(projectService.findAll(userId));
    }

    @RequestMapping(value = "/merge", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProjectDTO> mergeProject(@PathVariable(name = "userId") String userId, @RequestBody ProjectDTO project) throws Exception {
        projectService.merge(userId, project.getId(), project);
        return ResponseEntity.ok(projectService.findOne(userId, project.getId()));
    }

    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity removeProject(@PathVariable(name = "userId") String userId, @PathVariable final String id) throws Exception {
        projectService.remove(userId, id);
        return ResponseEntity.ok("Ok");
    }

    @RequestMapping(value = "/removeAll", method = RequestMethod.DELETE)
    public ResponseEntity removeAllProject(@PathVariable(name = "userId") String userId) throws Exception {
        projectService.removeAll(userId);
        return ResponseEntity.ok("Ok");
    }
}