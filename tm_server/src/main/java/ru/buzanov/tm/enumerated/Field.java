package ru.buzanov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlEnumValue;

public enum Field {
    @XmlEnumValue(value = "name")
    NAME("name"),
    @XmlEnumValue(value = "description")
    DESCRIPTION("description"),
    @XmlEnumValue(value = "startDate")
    START_DATE("startDate"),
    @XmlEnumValue(value = "finishDate")
    FINISH_DATE("finishDate");

    @NotNull
    private String name;

    Field(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }
}
