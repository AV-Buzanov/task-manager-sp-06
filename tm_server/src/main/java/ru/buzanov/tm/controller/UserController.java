package ru.buzanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.UserService;

import java.util.Arrays;

@Controller
@SessionAttributes(value = "user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private ProjectService projectService;

    @GetMapping("/users")
    public String userList(final Model model) throws Exception {
        model.addAttribute("list", userService.findAll());
        return "user/user";
    }

    @GetMapping(value = "/userview")
    public String userView(final Model model, @RequestParam(name = "id") final String id) throws Exception {
        final UserDTO user = userService.findOne(id);
        model.addAttribute("user", user);
        model.addAttribute("projects", projectService.findAll(id));
        return "user/userView";
    }

    @GetMapping(value = "/useredit")
    public String userEdit(final Model model, @RequestParam(name = "id") final String id) throws Exception {
        model.addAttribute("user", userService.findOne(id));
        model.addAttribute("roles", Arrays.asList(RoleType.values()));
        return "user/userEdit";
    }

    @PostMapping(value = "/usermerge")
    public String userMerge(@ModelAttribute(name = "user") final UserDTO user) throws Exception {
        userService.merge(user.getId(), user);
        return "redirect:/users";
    }

    @GetMapping(value = "/userremove")
    public String userRemove(@RequestParam(name = "id") final String id) throws Exception {
        userService.remove(id);
        return "redirect:/users";
    }

    @GetMapping(value = "/usercreate")
    public String userCreate(final Model model) throws Exception {
        final UserDTO user = new UserDTO();
        model.addAttribute("user", user);
        model.addAttribute("roles", Arrays.asList(RoleType.values()));
        return "user/userEdit";
    }
}
